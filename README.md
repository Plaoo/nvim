# Configurazione NeoVim

## Tutte le volte che fai un pull da questo repo ricordati di dare i seguenti comandi:

:PlugClean

```
Rimuove eventuali plugin che non servono
```

:PlugInstall

```
Installa i nuovi plugin presenti nel file di configurazione
```

## Comandi movimento

Ctrl+l e Ctrl+h
```
Si muove a destra e a sinistra nella tabs
```

F3
```
Apertura e chiusura file manager (NERDTree)
```

Spazio + pp
```
NERDTree nella directory del file aperto
```

Ctrl+d
```
Chiusura buffer
```

Ctrl+w s Ctrl+w v
```
Split orizzontale e split verticale
```

Ctrl+n
```
Una volta SELEZIONATA una parola ricerca e multicursore
```

## Comandi Magit
Spazio + gg
```
Apre la finestra magit
```

Ctrl+n
```
Si sposta nei hunks
```

S
```
Va in stage
```

CC
```
Scrive messaggio di commit
```
CC
```
DOPO aver scritto il commit lo conferma
```

## VimWiki
Spazio ww
```
Avvia VimWiki
[[nome]] per create una pagina
Tab per muoverti nell'indice
```
Ctrl p
```
Markdown preview
```
