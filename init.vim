set linebreak	" Break lines at word (requires Wrap lines)
set showbreak=~~ 	" Wrap-broken line prefix
set textwidth=80	" Line wrap (number of cols)
set showmatch	" Highlight matching brace
set visualbell	" Use visual bell (no beeping)
set number relativenumber
set tw=0
set hlsearch	" Highlight all search results
set smartcase	" Enable smart-case search
set ignorecase	" Always case-insensitive
set incsearch	" Searches for strings incrementally
"autocmd ColorScheme * highlight Normal ctermbg=NONE guibg=NONE
set autoindent	" Auto-indent new lines
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=4	" Number of spaces per Tab
let python_highlight_all=1
let g:fzf_buffers_jump = 1
filetype indent plugin on
syntax on
set ruler	" Show row and column ruler information
set cursorline
"Set per YouCompleteMe
let g:ycm_autoclose_preview_window_after_insertion = 1
set completeopt+=preview
set undolevels=4000	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour
let g:go_fmt_autosave = 0
let g:airline_section_b = '%{strftime("%H:%M")}'
"leader options
let NERDTreeWinSize=55
" enable line numbers
let NERDTreeShowLineNumbers=1
" make sure relative line numbers are used
autocmd FileType nerdtree setlocal relativenumber

noremap <Space> <Nop>
let mapleader=' '
"Commands
nnoremap <C-b>       :Buffers<CR>
nnoremap <Leader>fs  :w<CR>
nnoremap <Leader>qq  :q!<CR>
nnoremap <Leader>gb  :Git blame<CR>
nnoremap <Leader>fsq :wq<CR>
nnoremap <Leader>gg  :Magit<CR>
nnoremap <Leader>bf  :Black<CR>
nnoremap <Leader>q'  ciw''<Esc>P
nnoremap <Leader>q"  ciw""<Esc>P
nnoremap <Leader>q(  ciw()<Esc>P
nnoremap <Leader>q{  ciw{}<Esc>P
nnoremap <Leader>q[  ciw[]<Esc>P
nnoremap <Leader>cc  :call Generate_comments() <CR>
nnoremap <Leader>ii  :e ~/.config/nvim/init.vim <CR>

nnoremap <Leader>vt  :VimwikiTable 2 2
au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 2>/dev/null
"GoToDefinition
nnoremap <Leader>gd  :YcmCompleter GoToDefinition<CR>

"Current path
nnoremap <Leader>nn  :NERDTreeToggle<CR>
"Current file
nnoremap <Leader>pp  :NERDTreeFind<CR>
nnoremap <Leader>n   :NERDTree
"Dir lavoro
nnoremap <Leader>nl   :NERDTree $HOME/lavoro/<CR>
nnoremap <F2>	     :TagbarToggle<CR>
nnoremap <C-d>	     :bd<CR>
"remove trailing whitespaces
nnoremap <F8>	     :%s/\s\+$//e
"tabs
set hidden
nnoremap <C-l>	     :bnext<CR>
nnoremap <C-h>       :bprev<CR>
nnoremap <C-p>	     :MarkdownPreview<CR>

call plug#begin()
"Polyglot
Plug 'sheerun/vim-polyglot'
"Go
"Plug 'fatih/vim-go'
"XML
Plug 'othree/xml.vim'
Plug 'sukima/xmledit'
"Zig
Plug 'ziglang/zig.vim'
"VimWiki
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'vimwiki/vimwiki'
Plug 'alvan/vim-closetag'
Plug 'Yggdroot/indentLine'
"Haskell
Plug 'neovimhaskell/haskell-vim' 
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'psf/black'
Plug 'godlygeek/tabular'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'sbdchd/neoformat'
Plug 'itchyny/lightline.vim'
Plug 'dracula/vim'
Plug 'chase/focuspoint-vim'
Plug 'preservim/nerdtree'
Plug 'mattn/emmet-vim'
Plug 'chriskempson/base16-vim'
"Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
"Rust
Plug 'rust-lang/rust.vim'
Plug 'jreybert/vimagit'
Plug 'vim-syntastic/syntastic'
Plug 'majutsushi/tagbar'
Plug 'simeji/winresizer' "Ctrl + e e poi hjkl
Plug 'ap/vim-buftabline'
Plug 'Valloric/YouCompleteMe'
"
Plug 'terryma/vim-multiple-cursors'
call plug#end()
set termguicolors
colorscheme base16-monokai
hi Search guibg=DarkGrey guifg=Black

" Snippets
let g:UltiSnipsExpandTrigger="<c-j>"


""" Vim Wiki
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
let vim_markdown_preview_github=0
let vim_markdown_preview_perl=0
let vim_markdown_preview_pandoc=1
let vim_markdown_preview_browser='firefox'
let vim_markdown_preview_use_xdg_open=1

""" FZF window
""" general

let g:fzf_layout = { 'window': 'call CreateCenteredFloatingWindow()' }
let $FZF_DEFAULT_OPTS="--reverse "

" use rg by default
if executable('rg')
  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
  set grepprg=rg\ --vimgrep
endif

function! CreateCenteredFloatingWindow()
    let width = min([&columns - 4, max([80, &columns - 20])])
    let height = min([&lines - 4, max([20, &lines - 10])])
    let top = ((&lines - height) / 2) - 1
    let left = (&columns - width) / 2
    let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}

    let top = "╭" . repeat("─", width - 2) . "╮"
    let mid = "│" . repeat(" ", width - 2) . "│"
    let bot = "╰" . repeat("─", width - 2) . "╯"
    let lines = [top] + repeat([mid], height - 2) + [bot]
    let s:buf = nvim_create_buf(v:false, v:true)
    call nvim_buf_set_lines(s:buf, 0, -1, v:true, lines)
    call nvim_open_win(s:buf, v:true, opts)
    set winhl=Normal:Floating
    let opts.row += 1
    let opts.height -= 2
    let opts.col += 2
    let opts.width -= 4
    call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
    au BufWipeout <buffer> exe 'bw '.s:buf
endfunction
" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

function! Generate_comments()
    let snippet = [
          \ '""" Return a random page.',
          \ '',
          \ '    Performs a GET request to the /page/random/summary endpoint.',
          \ '',
          \ '    Args:',
          \ '        language: The Wikipedia language edition. By default, the English',
          \ '',
          \ '    Returns:',
          \ '        A page resource.',
          \ '',
          \ '    Raises:',
          \ '        ClickException: The HTTP request failed or the HTTP response',
          \ '        contained an invalid body.',
          \ '"""']
    call append(line('.'), snippet)
endfunction
